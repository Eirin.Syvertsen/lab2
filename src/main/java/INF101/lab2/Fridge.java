package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    int max_size = 20;
    ArrayList<FridgeItem> content;

    public Fridge() {
        content = new ArrayList<FridgeItem>(max_size);
    }
    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return content.size();
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge() < totalSize()) {
            content.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (content.contains(item)) {
            content.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        content.clear();        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (int i = 0; i < nItemsInFridge(); i++) {
            FridgeItem item = content.get(i);
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for (FridgeItem expiredItem : expiredFood) {
            content.remove(expiredItem);
        }
        
        return expiredFood;
    }
    

}
